import pygame

class Drawable:
    def __init__(self, x, y, (r,g,b), screen):
        self._x = x
        self._y = y
        self._colour = (r,g,b)
        self.screen = screen

    def getPos(self):
        return (self._x,self._y)

    def getColour(self):
        return self._colour

    def setPos(self, x, y):
        self._x = x
        self._y = y
    def setPos(self, (x, y)):
        self._x = x
        self._y = y

    def setColour(self, (r,g,b)):
        self._colour = (r,g,b)

    def draw(self):
        try:
            pygame.draw.circle(self.screen, self._colour, (self._x, self._y), 20, 0)
        except TypeError:
            pass

class Ball(Drawable):
    def draw(self):
        try:
            pygame.draw.circle(self.screen, self._colour, (self._x, self._y), 10, 0)
        except TypeError:
            pass

class Robot(Drawable):
    def draw(self):
        width = 100
        height = 60
        try:
            top = self._y-(height/2)
            bottom = self._y+(height/2)
            left = self._x-(width/2)
            right = self._x+(width/2)
            points = [(left, top), (left, bottom), (right, bottom), (right, top)]
            pygame.draw.polygon(self.screen, self._colour, points)
        except TypeError:
            pass
class Pitch(Drawable):
    def draw(self):
        try:
            w = self._x
            h = self._y
            hw = w/2
            hh = h/2
            z = -2
            pygame.draw.polygon(self.screen, self._colour, ((z,z),(z,h),(w,h),(w,z)), 10)
            pygame.draw.line(self.screen, self._colour, (hw, 0), (hw, h), 5)
            pygame.draw.circle(self.screen, self._colour, (hw, hh), 20, 0)
        except TypeError:
            pass
