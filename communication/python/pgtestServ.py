import socket
import readline
import sys

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((socket.gethostname(), int(sys.argv[1])))
serversocket.listen(1)

(clientsocket, addr) = serversocket.accept()
done = False
while not done:
    while 1:
        try:
            line = raw_input("Insert text to send: ")
            if not line:
                done = True
                break
            clientsocket.send(line)
            if line == "exit":
                done = True
                break
        except EOFError:
            done = True
            break
clientsocket.close()
serversocket.close()
serversocket.shutdown()
