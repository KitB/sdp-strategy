# VisionClient provides a high-level interface to a socket IPC system

# When instantiated this class will spin off a thread that will listen on the socket, updating positions for each object as it goes.

import socket
import re
from multiprocessing import Process, Array, Value

MESSAGE_LENGTH=10

class VisionClient:
    def __init__(self, host, tryPort):
        p = tryPort
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connected = False
        while not connected:
            try:
                s.connect((host, p))
                connected = True
            except socket.error:
                if not p-tryPort > 20:
                    p += 1
                else:
                    raise
        self._socket = s
        self._objects = dict()
        self._objects['b'] = Array('i', range(2))
        self._objects['y'] = Array('i', range(2))
        self._objects['r'] = Array('i', range(2))
        self._objects['d'] = Array('i', range(2))
        self._done = Value('B', False)
        self._p = Process(target=self._listenSocket, args=(self._objects['b'], self._objects['y'], self._objects['r'], self._objects['d'], self._done))
        self._p.start()
        #self._listenSocket(self._objects['b'], self._objects['y'], self._objects['r'], self._objects['d'], self._done)

    def _listenSocket(self, b, y, r, d, done):
        prog = re.compile("([byrd])\((\d{3}),(\d{3})\).*")
        while not done.value:
            try:
                data = self._socket.recv(MESSAGE_LENGTH)
            except socket.error:
                break
            if data == 'exit':
                print "Server detached"
                done.value = True
                break
            if not data:
                print "Server gone"
                done.value = True
                break

            m = prog.match(data)
            if m != None:
                type = m.group(1)
                mx = int(m.group(2))
                my = int(m.group(3))
                if type == 'b':
                    b[0] = mx
                    b[1] = my
                elif type == 'y':
                    y[0] = mx
                    y[1] = my
                elif type == 'r':
                    r[0] = mx
                    r[1] = my
                elif type == 'd':
                    d[0] = mx
                    d[1] = my

    def getSocket(self):
        return self._socket

    def getBlueBot(self):
        return self._objects['b']
    def getYellowBot(self):
        return self._objects['y']
    def getBall(self):
        return self._objects['r']
    def getDimensions(self):
        return self._objects['d']
    def getAll(self):
        return self._objects
    def stop(self):
        self._done.value = True
    def isDone(self):
        return self._done.value


if __name__ == '__main__':
    import pygame.time
    vc = VisionClient('kilmore.inf.ed.ac.uk', 31410)
    while True:
            pygame.time.Clock().tick(10)
            print "b(%d,%d)"%vc.getBlueBot()
