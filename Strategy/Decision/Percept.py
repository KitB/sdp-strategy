from math import hypot, pi

from ..Funcs import vectorDistance, angleBetween, relativeAngle, botDistance
from ..Model.World import *
from ..Params import Params


class Percept(object):
    def __init__(self):
        self._percepts = list()
        self._negated = False

    def _check(self, world):
        """ Return whether the percept is true in world """
        return True

    def __call__(self, strategy):
        """ Check a compound percept """
        for p in self._percepts:
            if not p(strategy):
                return False
        return (self._check(strategy) != self._negated)

    def negate(self):
        self._negated = not self._negated
        return self

    def add(self, percept):
        """ Add a new percept to this. """
        # I was able to form recursive percepts that would exhaust the stack prior to adding this line
        assert self not in percept._percepts, "Recursive percept detected, aborting"
        self._percepts.append(percept)
        # Return self so we can chain calls to add and initialise with them
        # Like so: p = HasBall().add(CanGoal()).add(InGoal())
        return self


class AlwaysTrue(Percept):
    def _check(self,strategy):
        return True

class HasBall(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        d = botDistance(world.getUs(), world.getBall())
        a = angleBetween(world.getUs(), world.getBall())   
        distTol = Params.hasBallDistTol
        angleTol = Params.hasBallAngleTol
             
        return (d < distTol) & (abs(a) < angleTol)    # TODO: move all params to .xml

class BallInMiddle(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        (w, h) = world.getDimensions()
        middle = (w/2, h/2)
        ball = world.getBall().getPosition()
        d = vectorDistance(middle, ball)
        return d < Params.ballInMiddleDistTol

class FacingGoal(Percept):
    # TODO: add a more complex check once the exact goal position is known
    def _check(self, strategy):
        world = strategy.getWorld()
        (w, h) = world.getDimensions()
        goal = (0, h/2)
        if world.getTheirSide() == "high":
            goal = (w, h/2)
        a = relativeAngle(world.getUs(), goal)
        return a < Params.facingGoalAngleTol

class CanGoal(Percept):
    def __init__(self):
        super(CanGoal, self).__init__()
        self.add(HasBall()).add(FacingGoal())

    def _check(self, strategy):
        world = strategy.getWorld()
        them = world.getThem()
        ballx, bally = world.getBall().getPosition()
        sideX = 0 if world.getTheirSide() == "low" else world.getDimensions()[0]

        # Magic number alert, might be worth putting goal info in some object.
        # These numbers are stolen from World.isInGoalbox()
        goalPointTop = (sideX, 253)
        goalPointBottom = (sideX, 90)

        corners = [them.getFrontLeft(), them.getFrontRight(), them.getBackLeft(), them.getBackRight()]
        highest, lowest = max(corners, key=lambda x: x[1]), min(corners, key=lambda x: x[1])

        intersectionTop = (float(bally - goalPointTop[1]) / (ballx - goalPointTop[0])) * (highest[0] - ballx) + bally
        intersectionBottom = (float(bally - goalPointBottom[1]) / (ballx - goalPointBottom[0])) * (lowest[0] - ballx) + bally

        topGap = intersectionTop - highest[1]
        bottomGap = lowest[1] - intersectionBottom
        
        ballGap = Params.canGoalBallGap
        
        return (topGap > ballGap or bottomGap > ballGap)

class InGoal(Percept):
    def __init__(self, robot=None, side=None):
        super(InGoal, self).__init__()
        self._robot = robot
        self._side = side
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.isInGoalbox(self._robot, self._side)

class UsInGoal(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.isInGoalbox(world.getUs(), world.getOurSide())

class ThemInGoal(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.isInGoalbox(world.getThem(), world.getTheirSide())

class HalfDistance(Percept):
    def __init__(self, initial):
        super(HalfDistance, self).__init__()
        self._initial = initial
        self._prev = initial
    def _check(self, strategy):
        world = strategy.getWorld()
        world.updateBlueRobot(*vc.getBlueBot())
        bot = world.getUs()
        ball = world.getBall()
        distance = vectorDistance(bot.getPosition(), ball.getPosition())
        angle = angleBetween(bot, ball)
        if distance > self._prev*1.1:
            print "Wrong movement"
            return True
        self._prev = distance
        return distance<=(self._initial/2.75)

class HasMovedEnoughToCalculateAngle(Percept): # Dayumn that's a long name
    def __init__(self, initPosition):
        super(HasMovedEnoughToCalculateAngle, self).__init__()
        self.initPosition = initPosition
    def _check(self, strategy):
        return vectorDistance(bot.getPosition(), self.initPosition)>1.5
