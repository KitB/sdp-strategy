from pygame.time import Clock

class Strategy:
    def __init__(self, world, robot):
        self._world = world
        self._robot = robot
        self._decisions = list()
        self._isDone = True

    def attach(self, decision):
        self._decisions.append(decision)

    def finish(self):
        self._isDone = True
        self._robot.stop()

    def getWorld(self):
        return self._world

    def getRobot(self):
        return self._robot

    def __call__(self):
        self._isDone = False
        clock = Clock()
        try:
            while not self._isDone:
                clock.tick(30)
                for decision in self._decisions:
                    # Loop through decisions until we find one that fires
                    if decision(self):
                        break
        except KeyboardInterrupt:
            print "User exited"
            self.finish()
