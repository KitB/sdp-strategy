import time
from math import degrees

from ..Funcs import angleBetween, simpleAngle
from ..Params import Params

def kickAndStop(strategy):
    strategy.getRobot().kick()
    strategy.finish()
def kick(strategy):
    strategy.getRobot().kick()
    time.sleep(1)
def dribble(strategy):
    r = strategy.getRobot()
    r.move(10,10)
    time.sleep(0.3)
    r.move(30,30)
    time.sleep(0.3)
    r.move(50,50)
    time.sleep(0.4)
    r.move(80,80)
    time.sleep(0.7)
    r.stop()
    strategy.finish()
def navigate(strategy):
    r = strategy.getRobot()
    w = strategy.getWorld()
    bot = w.getUs()
    ball = w.getBall()
    angle = angleBetween(bot, ball)
    print "Angle: " + degrees(angle)
    if abs(angle) > Params.navigateAngleTol:
        print "turning"
        r.turn(angle)
        time.sleep(1)
    else:
        print "going"
        r.move(40, 40)
        
def printVisionData(strategy):
    w = strategy.getWorld()
    print "Yellow Bot  Position: " + str(w.getYellowRobot().getPosition()) \
     + ",  \tOrientation: " + str(degrees(w.getYellowRobot().getAngle())) \
     + "\nBlue Bot    Position: " + str(w.getBlueRobot().getPosition()) \
     + ",  \tOrientation: " + str(degrees(w.getYellowRobot().getAngle())) \
     + "\nBall        Position: " + str(w.getBall().getPosition()) + "\n"
