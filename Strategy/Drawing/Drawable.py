import operator
from sys import maxint
from math import *

import pygame

from ..Funcs import *
from Strategy.LocalExceptions import abstract

# Some colours
red    = (239,  39,  19)
green  = ( 70, 111,  32)
blue   = ( 12, 170, 205)
yellow = (234, 234,  45)
white  = (255, 255, 255)

def drawPitch(screen, w, h):
    # Draw the pitch
    hw = w/2
    hh = h/2
    z = -2
    pygame.draw.polygon(screen, white, ((z,z),(z,h),(w,h),(w,z)), 10)
    pygame.draw.line(screen, white, (hw, 0), (hw, h), 5)
    pygame.draw.circle(screen, white, (hw, hh), 20, 0)

def drawRobot(screen, colour, x, y, a):
    width = 100
    height = 60
    try:
        top = y-(height/2)
        bottom = y+(height/2)
        left = x-(width/2)
        right = x+(width/2)
        x2 = x+((width/2)*cos(float(a)))
        y2 = y+((width/2)*sin(float(a)))
        middle = (x, y)
        points = map( lambda point: rotateAbout(point, middle, a)
                    , [(left, top), (left, bottom), (right, bottom), (right, top)]
                    )
        pygame.draw.polygon(screen, colour, points)
        pygame.draw.line(screen, white, middle, (x2, y2), 2)
        pygame.draw.circle(screen, darken(colour), middle, 10, 0)
    except TypeError:
        # We'll ignore if None happens, it will sometimes
        pass

def drawBall(screen, x, y):
    try:
        pygame.draw.circle(screen, red, (x, y), 10, 0)
    except TypeError:
        # These happen, we ignore them
        pass

def darken((R, G, B)):
    return (R/4, G/4, B/4)

def redraw(screen, bot, ball):
    screen.fill((70, 111, 32))
    ball.draw()
    bot.draw()
    pygame.display.update()

class Drawable:
    def __init__(self):
        abstract()
    def draw(self, screen):
        abstract()
