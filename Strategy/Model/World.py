import pygame

from Strategy.Model.WorldObjects import *
from Strategy.Drawing.Drawable import Drawable

# TODO: Add small object history perhaps?

white = (255,255,255)

class World(Drawable):

    def __init__(self, visionSource, DEBUG=False):
        self._vc = visionSource
        self._blueRobot = Robot(visionSource, 'blue')
        self._yellowRobot = Robot(visionSource, 'yellow')
        self._ball = Ball(visionSource)
        (self._w, self._h) = visionSource.getDimensions()
        self._DEBUG = DEBUG
        if DEBUG:
            print "Initialising pygame for debug"
            pygame.init()
            self._screen = pygame.display.set_mode((self._w, self._h))
            pygame.display.set_caption("Strategy Debug: Redraw vision")

    def setPlayer(self, player):
        assert player in ['b', 'y'], "Player must be \"b\" or \"y\"; given \"%s\""%player
        self._player = player
        self._us = self._blueRobot if player == 'b' else self._yellowRobot
        self._them = self._blueRobot if player == 'y' else self._yellowRobot

    def setSide(s, side):
        assert side in ["low", "high"], "Side must be \"low\" or \"high\" (based on x-value); given \"%s\""%side
        s._side = side

    def getOurSide(self):
        return self._side

    def getTheirSide(self):
        if self._side == "high":
            return "low"
        else:
            return "high"

    def getUs(self):
        self.doDraw()
        if self._us:
            return self._us
        else:
            return None

    def getThem(self):
        self.doDraw()
        if self._them:
            return self._them
        else:
            return None

    def getBlueRobot(self):
        self.doDraw()
        return self._blueRobot

    def getYellowRobot(self):
        self.doDraw()
        return self._yellowRobot

    def getBall(self):
        self.doDraw()
        return self._ball

    def getDimensions(self):
        return (self._w, self._h)

    def isInGoalbox(self, bot=None, side=None):
        GoalLength = 90 # Length of goal box area
        GoalWidth = 163 # Width of goal
        GoalY = 90      # Y position of start of goal

        # Set default values
        if side == None:
            side = self._side
        if bot == None:
            bot = self.getUs()

        (x, y) = bot.getPosition()

        vertical = y>GoalY and y<GoalY+GoalWidth
        if side == "low":
            horizontal = x < GoalLength
        elif side == "high":
            horizontal = x > self.w-GoalLength

        return horizontal and vertical

    def doDraw(self):
        if self._DEBUG:
            self.draw()
    def draw(self):
        screen = self._screen
        # Draw the background
        screen.fill((70, 111, 32))

        # Draw the pitch
        (w,h) = (self._w, self._h)
        hw = w/2
        hh = h/2
        z = -2
        pygame.draw.polygon(screen, white, ((z,z),(z,h),(w,h),(w,z)), 10)
        pygame.draw.line(screen, white, (hw, 0), (hw, h), 5)
        pygame.draw.circle(screen, white, (hw, hh), 20, 0)

        # Draw the objects
        try:
            self._blueRobot.draw(screen)
        except IndexError:
            pass

        try:
            self._ball.draw(screen)
        except IndexError:
            pass

        try:
            self._yellowRobot.draw(screen)
        except IndexError:
            pass

        pygame.display.update()
