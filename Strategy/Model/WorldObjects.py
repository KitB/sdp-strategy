from collections import deque
from math import cos, sin
import operator

import pygame

from Strategy.Funcs import rotateAbout, vecDist, vecAngle
from Strategy.Drawing.Drawable import Drawable


class WorldObject:

    def __init__(self, visionSource, obj):
        assert obj in ['b', 'y', 'r'], "Expected obj as ['b', 'y', 'r'], got: '%s'"%obj
        self._vc = visionSource
        self.positions = deque(maxlen=5)
        self._obj = obj

    def _update(self):
        if self._obj == 'b':
            self.positions.append(self._vc.getBlueBot())
        elif self._obj == 'y':
            self.positions.append(self._vc.getYellowBot())
        elif self._obj == 'r':
            self.positions.append(self._vc.getBall())

    def getPosition(self, update=True):
        if update:
            self._update()
        return self.positions[-1]

    def getPreviousPosition(self, n=1):
        assert n <= 5 and n > 0, "Expected n in range 1-5"
        return self.positions[-n]

class Robot(WorldObject, Drawable):

    def __init__(self, visionSource, colour):
        assert colour in ['blue', 'yellow'], "Robots can only be blue or yellow, given \"%s\""%colour
        if colour == 'blue':
            WorldObject.__init__(self, visionSource, 'b')
            self._colour = ( 12,170,205) # Blue colour
        elif colour == 'yellow':
            WorldObject.__init__(self, visionSource, 'y')
            self._colour = (234,234, 45) # Yellow colour

    def getAngle(self):
        return self.positions[-1][2]

    def getPreviousAngle(self, n=1):
        assert n <= 5 and n > 0, "Expected n in range 1-5"
        return self.positions[-n][2]

    def getPosition(self, update=True):
        if update:
            self._update()
        return self.positions[-1][0:2]

    def getPreviousPosition(self, n=1):
        assert n <= 5 and n > 0, "Expected n in range 1-5"
        return self.positions[-n][0:2]

    def draw(self, screen):
        width = 100
        height = 60
        (x, y) = self.getPosition(False)
        try:
            top = y-(height/2)
            bottom = y+(height/2)
            left = x-(width/2)
            right = x+(width/2)
            a = self.getAngle()
            x2 = x+(40*cos(float(a)))
            y2 = y+(40*sin(float(a)))
            middle = (x, y)
            points = map( lambda point: rotateAbout(point, middle, a)
                        , [(left, top), (left, bottom), (right, bottom), (right, top)]
                        )
            pygame.draw.polygon(screen, self._colour, points)
            pygame.draw.line(screen, (255, 255, 255), middle, (x2, y2), 2)
        except TypeError:
            # We'll ignore if None happens, it will sometimes
            pass


class Ball(WorldObject, Drawable):

    def __init__(self, visionSource):
        WorldObject.__init__(self, visionSource, 'r') 
        self._colour = (239, 39, 19)

    def draw(self, screen):
        try:
            pygame.draw.circle(screen, self._colour, self.getPosition(False), 10, 0)
        except TypeError:
            # These happen, we ignore them
            pass
