import numpy
import heapq

class Node:

    def __init__(self, x, y):
        self._x = x
        self._y = y
        self._heuristicCost = 0
        self._movementCost = 0
        self._totalCost = 0
        self._parent = None
        self._blacklist = False

    def x(self):
        return self._x

    def y(self):
        return self._y

    def getHeuristicCost(self):
        return self._heuristicCost

    def getMovementCost(self):
        return self._movementCost

    def getTotalCost(self):
        return self._totalCost

    def getParent(self):
        return self._parent

    def setHeuristicCost(self, value):
        self._heuristicCost = value

    def setMovementCost(self, value):
        self._movementCost = value

    def setTotalCost(self, value):
        self._totalCost = value

    def setParent(self, node):
        self._parent = node
        
    def isBlackListed(self):
        return self._blacklist

    def blacklist(self, value):
        self._blacklist = value

    def toCoords(self, gridWidth, gridHeight):

        """ Returns the center point of the node (grid square). """ 

        x = (self._x * gridWidth) + gridWidth / 2.0
        y = (self._y * gridHeight) + gridHeight / 2.0
        return x, y


class Graph:
    
    def __init__(self, pitchWidth, pitchHeight, width, height):
        self._pitchWidth = int(pitchWidth) # Safe guarding as non-integer division will cause unexpected results. 
        self._pitchHeight = int(pitchHeight)
        self._gridSquareWidth = self._pitchWidth / width
        self._gridSquareHeight = self._pitchHeight / height
        self._width = width
        self._height = height
        self._grid = numpy.array([[Node(x,y) for y in range(height)] for x in range(width)], object)


    def getNode(self, x, y):

        """ Points on the bound were causing out of array index errors due
        to grid squares starting at 0. The greater than is not strictly
        necessary but gives more room for error. """

        if x >= self._pitchWidth: 
            gridx = self._width - 1
        else:
            gridx = x / self._gridSquareWidth

        if y >= self._pitchHeight:
            gridy = self._height - 1
        else:
            gridy = y / self._gridSquareHeight

        return self._grid[gridx][gridy]


    def getNeighbours(self, node):
        x, y = node.x(), node.y()
        neighbours = []
        for i in range(x - 1, x + 2):
            for j in range(y - 1, y + 2):
                if (i >= 0 and j >= 0 and i < self._width and 
                    j < self._height and (i != x or j != y)):
                    neighbours.append(self._grid[i][j])
        return neighbours


    def calculateMovementCost(self, start, end):
        for node in self.getNeighbours(end):
            if node.isBlackListed():
                return 65
        if(start.x() == end.x() or start.y() == end.y()):
            return 10
        else:
            return 15
    
    def getPath(self, c):
        
        """ Returns the path in order excluding the very first and
        last nodes. This is because they are derived from actual
        co-ordinates so it would be more accurate to use the original
        start and goal points. """
        path = [c]
        while c.getParent() is not None:
            c = c.getParent()
            path.append(c)
        path.reverse()
        return path
       

    def printPath(self, path):
        for node in path:
            print "Node (%d,%d)" % (node.x(), node.y())

    def findPath(self, current, end):
        openSet = set()
        openHeap = []
        heapq.heapify(openHeap)
        closedSet = set()
        openSet.add(current)
        openHeap.append((current.getTotalCost(),current))

        while openSet:
            current = heapq.heappop(openHeap)[1]

            if current == end:
                return current

            openSet.remove(current)
            closedSet.add(current)

            for gridPoint in self.getNeighbours(current):

                if not(gridPoint.isBlackListed()):
                    cost = current.getMovementCost() + self.calculateMovementCost(current, gridPoint)
                    
                    if gridPoint not in closedSet:

                        # Heuristic here is Chebyshev Distance
                        gridPoint.setHeuristicCost(max(abs(gridPoint.x() - end.x()), 
                                                       abs(gridPoint.y() - end.y()))*10)

                        if gridPoint not in openSet:
                            openSet.add(gridPoint)
                            gridPoint.setMovementCost(cost)
                            gridPoint.setTotalCost(gridPoint.getMovementCost() + gridPoint.getHeuristicCost())
                            heapq.heappush(openHeap, (gridPoint.getTotalCost(), gridPoint))
                            gridPoint.setParent(current)
        return []
