from Strategy.Model.World import World
from Strategy.Model.WorldObjects import *
from Strategy.Communication.VisionClient import DummyVision, dummyVals

dummyWorld = World(DummyVision(dummyVals))

def test_dummyworld():
    v = dict()
    (bx, by) = dummyWorld.getBlueRobot().getPosition()
    ba = dummyWorld.getBlueRobot().getAngle()
    v['blue'] = (bx, by, ba)
    (yx, yy) = dummyWorld.getYellowRobot().getPosition()
    ya = dummyWorld.getYellowRobot().getAngle()
    v['yellow'] = (yx, yy, ya)
    v['ball'] = dummyWorld.getBall().getPosition()
    v['dims'] = dummyWorld.getDimensions()
    assert v == dummyVals
