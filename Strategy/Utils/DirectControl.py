#!/usr/bin/env python

import pygame
from pygame.locals import *
from Communication import *

pygame.init()
screen = pygame.display.set_mode((640,480))
robot = RobotControl()

def forwards():
    global l
    global r
    (l, r) = 70, 70

def backwards():
    global l
    global r
    (l, r) = -70, -70

def left():
    global l
    global r
    r = 127

def right():
    global l
    global r
    l = 127

def move():
    global pl, pr, l, r
    global stopped
    stopped = (l,r) == (0,0)
    if (pl, pr) != (l, r):
        robot.move(l,r)
        pl, pr = l, r

global pl, pr, l, r
l, r = 0,0
pl, pr = 0,0
done = False
clock = pygame.time.Clock()
stopped = True
while not done:
    clock.tick(25)
    pygame.event.get()
    pressed = pygame.key.get_pressed()
    if pressed[K_w]:
        forwards()
    if pressed[K_s]:
        backwards()
    if pressed[K_a]:
        left()
    if pressed[K_d]:
        right()
    if not (pressed[K_w] or pressed[K_a] or pressed[K_s] or pressed[K_d]):
        if not stopped:
            stopped = True
            pl, pr = 0,0
            l, r = 0,0
            robot.stop()
    move()
