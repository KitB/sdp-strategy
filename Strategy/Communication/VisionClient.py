# VisionClient provides a high-level interface to a socket IPC system

# When instantiated this class will spin off a thread that will listen on the socket, updating positions for each object as it goes.

import socket
import struct
from multiprocessing import Process, Array, Value
from math import pi

from Strategy.LocalExceptions import AbstractClassInstantiatedError, abstract

MESSAGE_LENGTH=12

RQ_BLUE = 1
RQ_YELLOW = 2
RQ_RED = 3
RQ_DIMENSIONS = 4

class VisionSource(object):
    """ An abstract class representing a source for vision data """
    def __init__(self):
        abstract(self)
    def getBlueBot(self):
        """ Return the position and angle of the blue robot """
        return self._blue
    def getYellowBot(self):
        """ Return the position and angle of the yellow robot """
        return self._yellow
    def getBall(self):
        """ Return the position of the ball """
        return self._ball
    def getDimensions(self):
        """ Return the dimensions of the pitch """
        return self._dims

dummyVals = { 'blue' : (230, 250, pi)
            , 'yellow' : (120, 360, pi/4)
            , 'ball' : (50, 70)
            , 'dims' : (631, 331)
            }

class DummyVision(VisionSource):
    """ A fake vision source that serves constant data """
    def __init__(self, v):
        self.blue = v['blue']
        self.yellow = v['yellow']
        self.ball = v['ball']
        self.dims = v['dims']
    def getBlueBot(self):
        return self.blue
    def getYellowBot(self):
        return self.yellow
    def getBall(self):
        return self.ball
    def getDimensions(self):
        return self.dims

class VisionClient(VisionSource):
    """ The real vision source that serves vision data acquired from a vision server """
    def __init__(self, host=None, tryPort=31410, usingSim=False):
        # We'll be trying up to twenty higher than the given port
        p = tryPort
        h = host if host != None else socket.gethostname()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connected = False
        while not connected:
            try:
                s.connect((h, p))
                connected = True
            except socket.error:
                if not p-tryPort > 20:
                    p += 1
                else:
                    raise
        self._socket = s

    def getSocket(self):
        """ Returns the socket we're listening on """
        return self._socket

    # Request functions {
    # TODO: Empty input stream before sending request
    def getBlueBot(self):
        self._socket.send(struct.pack('b', RQ_BLUE))
        (type, x, y, a) = self.receive()
        assert type == 'b', "Wrong type returned: expected 'b'; given %s"%type
        return (x, y, a+pi)

    def getYellowBot(self):
        self._socket.send(struct.pack('b', RQ_YELLOW))
        (type, x, y, a) = self.receive()
        assert type == 'y', "Wrong type returned: expected 'y'; given %s"%type
        return (x, y, a+pi)

    def getBall(self):
        self._socket.send(struct.pack('b', RQ_RED))
        (type, x, y, _) = self.receive()
        assert type == 'r', "Wrong type returned: expected 'r'; given %s"%type
        return (x, y)

    def getDimensions(self):
        self._socket.send(struct.pack('b', RQ_DIMENSIONS))
        (type, x, y, _) = self.receive()
        assert type == 'd', "Wrong type returned: expected 'd'; given %s"%type
        return (x, y)

    def receive(self):
        data = self._socket.recv(MESSAGE_LENGTH)
        assert len(data) == 12, "Wrong length, expected 12 given %d"%len(data)
        return struct.unpack('cHHf', data)
    # }

if __name__ == '__main__':
    import pygame.time
    vc = VisionClient('kilmore.inf.ed.ac.uk', 31410, False)
    while True:
            pygame.time.Clock().tick(10)
            print "b(%d,%d)"%vc.getBlueBot()
