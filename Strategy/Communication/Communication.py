#!/usr/bin/env python

import socket
import array

TURN_FACTOR = 1.44444444444444

class RobotControl:
    def __init__(self, host=None, port=None):
        self.__HOST = host if not host == None else socket.gethostname()
        self.__PORT = port if not port == None else 6789
        self.__MOVE = 1
        self.__KICK = 2
        self.__TURN = 3
        self.__STOP = 4
        self.__EXIT = 99

        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.__socket.connect((self.__HOST, self.__PORT))
        except socket.error as (no, str):
            print "Couldn't connect to socket: %s"%str

    def forwards(self):
        #print "Going forwards"
        motor_speed = 40
        self.move(motor_speed, motor_speed)

    def backwards(self):
        #print "Going backwards"
        motor_speed = -20
        self.move(motor_speed, motor_speed)

    def stop(self):
        #print "Stopping"
        command = array.array('b', [self.__STOP, 0, 0, 0, 0])
        self.__socket.send(command)

    def move(self, left, right):
        #print "Moving"
        command = array.array('b', [self.__MOVE, left, right, 0, 0])
        self.__socket.send(command)

    def turn(self, angle):
        #print "Turning by %f"%angle
        angle = int(degrees(angle))
        command = array.array('B', [self.__TURN, (angle>>8)&255, (angle)&255, 0, 0])
        self.__socket.send(command)

    def turn_around(self):
        #print "Turning around"
        angle = 260 # this value should turn the robot ~180 degrees
        self.turn(angle)

    def kick(self):
        #print "Kicking"
        command = array.array('b', [self.__KICK, 0, 0, 0, 0])
        self.__socket.send(command)

    def exit(self):
        #print "Exiting"
        command = array.array('b', [self.__EXIT, 0, 0, 0, 0])
        self.__socket.send(command)

class FakeRobot:
    def __init__(self, host=None, port=None):
        print "Initialising robot"

    def forwards(self):
        print "Going forwards"

    def backwards(self):
        print "Going backwards"

    def stop(self):
        print "Stopping"

    def turn_around(self):
        print "Turning around"

    def kick(self):
        print "Kicking"

    def move(self, left, right):
        print "Moving: (%d, %d)"%(left, right)

    def turn(self, angle):
        print "Turning by angle %f"%angle

    def exit(self):
        print "Exiting"
