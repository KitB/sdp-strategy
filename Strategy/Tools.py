import math 

def rotate2d(radians,point,origin):
    
    """ A rotation function that rotates a point around a point
    to rotate around the origin use [0,0] """

    x = point[0] - origin[0]
    yorz = point[1] - origin[1]
    newx = round((x * math.cos(radians)) - (yorz * math.sin(radians)))
    newyorz = round((x * math.sin(radians)) + (yorz * math.cos(radians)))
    newx += origin[0]
    newyorz += origin[1] 

    return newx,newyorz
    
    # I don't understand how this function differs from Funcs.rotateAbout(vector, point, angle). Where is it used in?
