# Miscellaneous functions
import operator
from math import hypot, atan2, sin, cos, pi

def vectorDistance((x1, y1), (x2, y2)):
    return hypot(x1-x2, y1-y2)

def botDistance(obj1, obj2):    # TODO: rename to objDistance
    return vectorDistance(obj1.getPosition(), obj2.getPosition())

def angleBetween(bot, ball):
    return relativeAngle(bot, ball.getPosition())
    
def relativeAngle(bot, point):
    orient = bot.getAngle()
    alpha = vecAngle(point,bot.getPosition())
    angle = simpleAngle(alpha-orient)
    return angle

def simpleAngle(a):
    while a >= pi:
        a -= 2*pi
    while a <= -pi:
        a += 2*pi
    return a

def vecAngle(a, b):
    try:
        v = map(operator.sub, a, b)
        return atan2(v[1], v[0])
    except TypeError:
        return 0

def vecDist(a, b):
    #print "%s\t%s"%(a, b)
    try:
        return hypot(*map(operator.sub, a, b))
    except TypeError as e:
        print e

def rotate(point, angle):
    (x, y) = point
    sint = sin(angle)
    cost = cos(angle)
    return (cost*x-sint*y, sint*x+cost*y)

def rotateAbout(vector, point, angle):
    return map(operator.add, point, rotate(map(operator.sub, vector, point), angle))

