# This file stores only parameters. If you want to use a parameter, import it in the file you want to use it in. For example: 'import Params', followed by circleDist = Params.circleDist in your code later on. 
from math import pi
import sys
from inspect import getsourcelines, getfile
import re

# Naming convention is function name + additional suffixes.
# List of additional suffixes:
#   -tol   : Tolerance
#   -dist  : Distance
#   -angle : Angle
#   -const : Constant

class ParameterStore:
    def writeFile(self):
        module = sys.modules[__name__]
        lines = getsourcelines(sys.modules[__name__])[0]
        # Parameter definition line regex
        IDENT = "[a-zA-Z_][a-zA-Z0-9_]*"
        pdlr = re.compile("^Params\.(%s) = (.*)$"%IDENT)
        str = ""
        for line in lines:
            m = pdlr.match(line)
            if m:
                varName = m.group(1)
                newVal = self.__dict__[varName]
                print "Setting %s to %s"%(varName, repr(newVal))
                line = "Params.%s = %s\n"%(varName, newVal)
            str += line
        f = open(getfile(module), 'w')
        f.write(str)

Params = ParameterStore()

# Percept
Params.hasBallDistTol = 50
Params.hasBallAngleTol = 0.392699081699
Params.ballInMiddleDistTol = 20
Params.facingGoalAngleTol = 0.785398163397
Params.canGoalBallGap = 15

# Actions
Params.navigateAngleTol = 1.0471975512


# Constants
Params.turningConst = 1.444444444
Params.pixelsToCm = 6.1
