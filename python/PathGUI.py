import pygame
import PathFinding
from pygame.locals import *




class PathFinderGUI:

    #           -1        1           2            3            4        Start    Finish    Find path         Reset
    colors = [(0,0,0),(0,255,0),(0,255-20,0),(0,255-40,0),(0,255-60,0),(0,0,255),(255,0,0),(100,150,100),(150,100,100)]

    def __init__(self, width, height, squareWidth, squareHeight):
        self.mapw = width
        self.maph = height
        self.squareWidth = squareWidth
        self.squareHeight = squareHeight
        self.grid = PathFinding.Graph(width*squareWidth, height*squareHeight, width, height)



    def initMap(self):
        self.mapdata = []
        w = self.mapw
        h = self.maph
        squareHeight = self.squareHeight
        squareWidth = self.squareWidth
        self.startpoint = [1,h/2]
        self.endpoint = [w-2,h/2]
        
        size = w*h;
        for i in range(size):
            self.mapdata.append(1)

        self.mapdata[(self.startpoint[1]*w)+self.startpoint[0]] = 5
        self.mapdata[(self.endpoint[1]*w)+self.endpoint[0]] = 6

        self.maprect = Rect(0,0,w*squareWidth,h*squareHeight)


    def drawMap(self):

        x = 0
        y = 0
        rect = [0,0,self.squareWidth,self.squareHeight]
        for p in self.mapdata:
            if p == -1:
                p = 0
            rect[0] = x*self.squareWidth
            rect[1] = y*self.squareHeight
            self.screen.fill(self.colors[p],rect)
            x+=1
            if x>=self.mapw:
                x=0
                y+=1

    def updateMap(self,mx,my,v):
        x = mx / self.squareWidth
        y = my / self.squareHeight
        mi = (y*self.mapw)+x
        if v == 5: # startpoint
            if self.mapdata[mi] != 5 and self.mapdata[mi] != 6:
                self.mapdata[(self.startpoint[1]*self.mapw)+self.startpoint[0]] = 1
                self.screen.fill(self.colors[1],(self.startpoint[0]*self.squareWidth,self.startpoint[1]*self.squareHeight,self.squareWidth,self.squareHeight))
                self.startpoint = [x,y]
                self.mapdata[mi] = 5
                self.screen.fill(self.colors[5],(x*self.squareWidth,y*self.squareHeight,self.squareWidth,self.squareHeight))
        elif v == 6: # endpoint
            if self.mapdata[mi] != 5 and self.mapdata[mi] != 6:
                self.mapdata[(self.endpoint[1]*self.mapw)+self.endpoint[0]] = 1
                self.screen.fill(self.colors[1],(self.endpoint[0]*self.squareWidth,self.endpoint[1]*self.squareHeight,self.squareWidth,self.squareHeight))
                self.endpoint = [x,y]
                self.mapdata[mi] = 6
                self.screen.fill(self.colors[6],(x*self.squareWidth,y*self.squareHeight,self.squareWidth,self.squareHeight))
        else:
            if self.mapdata[mi] != 5 and self.mapdata[mi] != 6:
                if v == 0:
                    self.mapdata[mi] = -1
                else:
                    self.mapdata[mi] = v
                self.grid.getNode(mx, my).blacklist(True)
                self.screen.fill(self.colors[v],(x*self.squareWidth,y*self.squareHeight,self.squareWidth,self.squareHeight))


    def runPathFinding(self):
        startNode = self.grid._grid[self.startpoint[0]][self.startpoint[1]]
        endNode = self.grid._grid[self.endpoint[0]][self.endpoint[1]]
        path = self.grid.findPath(startNode, endNode)
        if path:
            path = self.grid.getPath(path)
            print "length %d" % (len(path))
            self.grid.printPath(path)
            self.drawPath(path)
        else:
            print "Path not found!"
        
    def drawPath(self, path):
        points = []
        for node in path:
            points.append(node.toCoords(self.squareWidth, self.squareHeight))
        pygame.draw.lines(self.screen, (255,255,255,255), 0, points)

    def loop(self):
        pygame.init()    

        self.screen = pygame.display.set_mode((self.squareWidth*self.mapw, self.squareHeight*self.maph),HWSURFACE)
        pygame.display.set_caption('PathFinder')
    
        self.screen.fill((150,150,150))
        
        self.initMap()
        self.drawMap()

        mode = -1

        while 1:
            for event in pygame.event.get():
                if event.type == QUIT:
                    return
                elif event.type == KEYDOWN:          
                    if event.key == K_ESCAPE:
                        return
                    elif event.key == K_RETURN:
                        self.runPathFinding()
                    elif event.key == K_s:
                        mode = 5 if mode != 5 else -1
                        print "Start Mode is %s" % ("ON" if mode == 5 else "OFF")
                    elif event.key == K_e:
                        mode = 6 if mode != 6 else -1
                        print "End Mode is %s" % ("ON" if mode == 6 else "OFF")
                elif event.type == MOUSEBUTTONDOWN:                    
                    mx = event.pos[0]
                    my = event.pos[1]
                    if self.maprect.collidepoint(mx,my):
                        self.updateMap(mx, my, mode)
                        print "Clicked %d, %d" % (mx, my)
                        self.drawMap()
                elif event.type == MOUSEMOTION and event.buttons[0]:            
                    mx = event.pos[0]
                    my = event.pos[1]
                    if self.maprect.collidepoint(mx,my):
                        self.updateMap(mx,my, mode)
                        self.drawMap()
            pygame.display.flip()





def main():
    gui = PathFinderGUI(16, 11, 40, 30)
    gui.loop()

if __name__ == '__main__': main()
